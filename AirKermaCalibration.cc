
#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4UIsession.hh"
#include "G4UIterminal.hh"
#include "G4UIcsh.hh"
#include "G4UIExecutive.hh"

#include "G4EmCalculator.hh"
#include "DetectorConstruction.hh"
#include "PhysicsList.hh"
#include "PrimaryGeneratorAction.hh"
#include "StackingAction.hh"
#include "G4VisExecutive.hh"
#include "RunAction.hh"
#include "EventAction.hh"
#include "Randomize.hh"
#include <iostream>
using namespace std;

int main(int argc, char** argv) {
  G4String fileName;
    /*
    long int Tid = 0;
    if (argc != 1) {
        fileName = argv[1];
        string timestring = argv[2];
        G4cout << " timestring: " << timestring << G4endl;
        char * pEnd;

        Tid = strtol(argv[2], &pEnd, 10);

        G4cout << " Tid:  " << Tid << G4endl;
    }
    */

    CLHEP::RanecuEngine *theRanGenerator = new CLHEP::RanecuEngine;
    //theRanGenerator->setSeed(Tid);
    CLHEP::HepRandom::setTheEngine(theRanGenerator);
    //G4cout <<Tid << G4endl;

    G4RunManager* runManager = new G4RunManager;

    G4int nVoxels = 1;
    DetectorConstruction* detector = new DetectorConstruction(nVoxels);
    runManager->SetUserInitialization(detector);

    runManager->SetUserInitialization(new PhysicsList);

    PrimaryGeneratorAction *thePrimaryGeneratorAction = new PrimaryGeneratorAction();
    RunAction *theRunAction = new RunAction(nVoxels, fileName);
    runManager->SetUserAction(thePrimaryGeneratorAction);
    runManager->SetUserAction(theRunAction);
    runManager->SetUserAction(new StackingAction(theRunAction));
    EventAction* pEventAction = new EventAction();
    runManager->SetUserAction(pEventAction);


G4UImanager* UI = G4UImanager::GetUIpointer();

#ifdef G4VIS_USE
    G4VisManager* visManager = new G4VisExecutive;
    visManager->Initialize();
#endif

    if (argc != 1) {
        G4String command = "/control/execute ";
        G4String file_Name = argv[1];
        UI->ApplyCommand(command+file_Name);
    } else {
#ifdef G4UI_USE
        G4UIExecutive * ui = new G4UIExecutive(argc, argv);
        ui->SessionStart();
        delete ui;
#endif
    }

#ifdef G4VIS_USE
    delete visManager;
#endif

    delete runManager;
    return 0;
}
