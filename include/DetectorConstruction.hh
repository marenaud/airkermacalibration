
#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

class DetectorMessenger;
class G4LogicalVolume;
class G4Material;
class G4Box;
class G4Orb;
class G4Colour;
class G4VPhysicalVolume;
class G4VPhysicalVolume;
class SourceMaterial;
class SourceModel;

#include "G4VUserDetectorConstruction.hh"
#include "G4Material.hh"

class DetectorConstruction : public G4VUserDetectorConstruction
{
	public:

  	DetectorConstruction(G4int NumberVoxels=100);
  	~DetectorConstruction();

    void SelectBrachySource(G4String);
    void SelectBrachyCore(G4String);
    void ConstructWorld();

  	G4VPhysicalVolume* Construct();
    G4Material* GetMaterial() {return theMaterial;};
    private:
    G4Material* theMaterial;


   // Logical volumes
    //
    G4LogicalVolume* World_log;

    // Physical volumes
    //
    G4VPhysicalVolume* World_phys;

    G4double ScoringSphereRadius;
    G4double WorldSize;

    SourceMaterial* pMaterial;
    DetectorMessenger* detectorMessenger;
    G4String fBrachySource;
    G4String fCore;
    SourceModel* Source;

  G4int Nvoxels;
};

#endif
