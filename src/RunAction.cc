#include <fstream>
#include <ctime>
#include <math.h>

#include "RunAction.hh"
#include "DetectorConstruction.hh"
#include "PrimaryGeneratorAction.hh"
#include "G4Run.hh"
#include "G4ProcessManager.hh"
#include "G4ProductionCutsTable.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"
#include "G4UImanager.hh"
#include "G4ios.hh"
#include "Run.hh"
#include "G4EmCalculator.hh"
#include "G4SystemOfUnits.hh"
#include "globals.hh"

using namespace std;

G4int RunAction::numEvents=0;

RunAction::RunAction(G4int NumberVoxels, G4String Name) {
    Nvoxels=NumberVoxels;
    RunName=Name;
    fNGammasCreated = 0;
}

RunAction::~RunAction() {}

void RunAction::BeginOfRunAction(const G4Run* aRun) {
  numEvents=aRun->GetNumberOfEventToBeProcessed();
  time_t mytime;
  time (&mytime);

  if (numEvents >= 100){
    G4cout << setw(9) << "     " << setw(8) << "Event No" << setw(22) << "Start Time" << G4endl;
    G4cout << " ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ " << G4endl;
  }
}

G4Run* RunAction::GenerateRun() {
  std::vector<G4String>theSDNames(1,"det");
  return new Run(theSDNames);
}

void RunAction::EndOfRunAction(const G4Run* aRun) {
  // Processed Number of Event
  G4int NbOfEvents = aRun->GetNumberOfEvent();
  G4cout << " Number of Events Processed:"
     << NbOfEvents << " events. " <<G4endl;

  Run *theRun=(Run*)aRun;
  theRun->DumpAllScorer();

  // Retriving Accumulatet data and output results.
  // Dump result for all HitsMap collections.
  //
  G4int NhitsMap = theRun->GetNumberOfHitsMap();
  for (G4int im=0; im<NhitsMap; im+=2) {
    G4THitsMap<G4double>*RunMap1 = theRun->GetHitsMap(im);
    G4THitsMap<G4double>*RunMap2 = theRun->GetHitsMap(im+1);
    if (RunMap1 && RunMap2) {
      G4String filename = RunName+"_"+RunMap1->GetName()+".csv";
      std::ofstream out(filename.data());
      out<<"Number of events processed," << NbOfEvents <<G4endl;
      out<<"Number of gammas created," << fNGammasCreated <<G4endl;
      out<<"radius (cm),Air kerma per particle *R*R (MeV cm2 g-1), Error"<<G4endl;
      for(G4int i=0;i<Nvoxels;i++) {
        G4double* dose = (*RunMap1)[i];
        G4double* error = (*RunMap2)[i];
        if (dose && error) {
          *dose = *dose / (G4double)fNGammasCreated * 5 * 5 * cm2;
          *error = *error / (G4double)fNGammasCreated * 5 * 5 * 5 * 5 * cm2 * cm2;
          *error = *error - (*dose)*(*dose);
          *error = *error / ((G4double)fNGammasCreated-1.0);
          *error=sqrt(*error);

          out <<"10"<<"," << *dose/(MeV/g)/cm2 <<","
          << *error/(MeV/g)/cm2 << G4endl;
        }
        //out <<i<<","<<G4BestUnit(*dose/(G4double)NbOfEvents, "Dose")<<G4endl;
      }
      out.close();
    }
  }
}
