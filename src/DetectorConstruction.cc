#include "DetectorConstruction.hh"

#include "SourceMaterial.hh"

#include "FlexiSource.hh"
#include "MicroSelectronV2.hh"

#include "DetectorMessenger.hh"
#include "G4SystemOfUnits.hh"
#include "globals.hh"
#include "G4Material.hh"
#include "G4Orb.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4UnionSolid.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4UnitsTable.hh"
#include "G4VisAttributes.hh"
#include "G4SDManager.hh"
#include "G4MultiFunctionalDetector.hh"
#include "PSDoseDeposit.hh"
#include "PSDoseAirDeposit.hh"
#include "G4NistManager.hh"
#include "G4UserLimits.hh"

#include "PSDoseDeposit.hh"
#include "PSDoseAirDeposit.hh"

DetectorConstruction::DetectorConstruction(G4int NumberVoxels)
 :  World_log(0), World_phys(0) {
  WorldSize=200.*cm;
  ScoringSphereRadius=5.*cm;
  Nvoxels=NumberVoxels;

  // FlexiSource is the default source in the geometry.
  fBrachySource = "FlexiSource";
  fCore = "G4_Ir";

  detectorMessenger = new DetectorMessenger(this);

  pMaterial = new SourceMaterial();
}

DetectorConstruction::~DetectorConstruction() {}

void DetectorConstruction::SelectBrachySource(G4String val) {
  fBrachySource = val;
  G4cout << "The source is set to " << val << G4endl;
}

void DetectorConstruction::SelectBrachyCore(G4String val) {
  fCore = val;
  G4cout << "The core is set to " << val << G4endl;
}

G4VPhysicalVolume* DetectorConstruction::Construct() {
  if (fBrachySource == "FlexiSource") {
    Source = new FlexiSource("", fCore);
  } else if (fBrachySource == "MicroSelectronV2") {
    Source = new MicroSelectronV2("", fCore);
  } else {
    Source = new FlexiSource("", fCore);
  }

  pMaterial->DefineMaterials();
  ConstructWorld();
  // Model the source in the world
  Source->CreateSource(World_phys);

  return World_phys;
}

void DetectorConstruction::ConstructWorld() {

  G4NistManager *man = G4NistManager::Instance();
  man->SetVerbose(2);
  G4Material* Air = man->FindOrBuildMaterial("G4_AIR");

  G4double density = CLHEP::universe_mean_density;  // 1.e-25*g/mole;
  G4double pressure = 3.e-18 * pascal;
  G4double a = 1.01 * g / mole;
  G4double temperature = 293.15 * kelvin;

  G4Material* Vacuum = new G4Material("Vacuum", 1., a, density, kStateGas,
   temperature, pressure);

  G4Box* World_box = new G4Box("World_box",WorldSize,WorldSize,WorldSize);
  World_log = new G4LogicalVolume(World_box, Vacuum, "World_log", 0, 0, 0);
  World_phys = new G4PVPlacement(0, G4ThreeVector(), World_log, "World", 0,
                false, 0);

  G4Sphere* Scoring_Sphere = new G4Sphere("Scoring Sphere", 49.99*mm, 50.01*mm, 0*deg, 360.*deg, 90.0*deg - 7.6*deg, 2.*7.6*deg);
  G4LogicalVolume* Scoring_log = new G4LogicalVolume(Scoring_Sphere, Air,"Scoring_log", 0, 0, 0);
  new G4PVPlacement(0, G4ThreeVector(0,0,0), Scoring_log, "Scoring shell", World_log, false, 0);

  G4SDManager* pSDManager = G4SDManager::GetSDMpointer();
  G4MultiFunctionalDetector* pMultiFunctionalDetector = new G4MultiFunctionalDetector("det");

  PSDoseAirDeposit* primitive2;
  primitive2 = new PSDoseAirDeposit(Nvoxels,ScoringSphereRadius ,"air_kerma_muen", 0);
  pMultiFunctionalDetector->RegisterPrimitive(primitive2);

  pSDManager->AddNewDetector(pMultiFunctionalDetector);
  Scoring_log->SetSensitiveDetector(pMultiFunctionalDetector);

  //------------------------------
  //Visualization attributes
  //------------------------------

  //World
  G4VisAttributes* World_visAtt = new G4VisAttributes;
  World_visAtt->SetVisibility(true);
  //World_visAtt->SetForceWireframe(true);
  World_visAtt->SetForceSolid(true);
  World_visAtt->SetDaughtersInvisible(false);
  World_visAtt->SetColor(0.7, 0., 0.7);
  Scoring_log->SetVisAttributes(World_visAtt);

  //Water sphere
  G4VisAttributes* WaterSphere_visAtt = new G4VisAttributes;
  WaterSphere_visAtt->SetVisibility(true);
  WaterSphere_visAtt->SetForceSolid(true);
  //WaterSphere_visAtt->SetForceWireframe(true);
  WaterSphere_visAtt->SetColor(0., 0., 0.7);

}
