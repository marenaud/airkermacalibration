
#include "DetectorMessenger.hh"
#include "DetectorConstruction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"


DetectorMessenger::DetectorMessenger( DetectorConstruction* Det): detector(Det) {
  detectorDir = new G4UIdirectory("/world/");
  detectorDir -> SetGuidance(" world control.");

  sourceCmd = new G4UIcmdWithAString("/source/switch", this);
  sourceCmd -> SetGuidance("Chose among the selected source geometries.");
  sourceCmd -> SetParameterName("choice", true);
  sourceCmd -> SetDefaultValue(" ");
  sourceCmd -> SetCandidates("MicroSelectronV2 FlexiSource ProsperaSeed MicroSelectronV2Se MicroSelectronV2Gd");
  sourceCmd -> AvailableForStates(G4State_PreInit, G4State_Idle);

  coreCmd = new G4UIcmdWithAString("/source/core", this);
  coreCmd -> SetGuidance("Chose among the following source cores.");
  coreCmd -> SetParameterName("choice", true);
  coreCmd -> SetDefaultValue(" ");
  coreCmd -> SetCandidates("G4_Ir G4_Gd G4_Se");
  coreCmd -> AvailableForStates(G4State_PreInit, G4State_Idle);
}

DetectorMessenger::~DetectorMessenger()
{
  delete sourceCmd;
  delete coreCmd;
  delete detectorDir;
}

void DetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  // Messenger function to set the source type
  if (command == sourceCmd) {
    if(newValue=="MicroSelectronV2" || newValue=="FlexiSource"|| newValue=="ProsperaSeed" || newValue == "MicroSelectronV2Se" || newValue == "MicroSelectronV2Gd") {
       detector->SelectBrachySource(newValue);
    }
  }

  if (command == coreCmd) {
    detector->SelectBrachyCore(newValue);
  }
}

